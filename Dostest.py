import paramiko
import threading
import re
import time
from printsytle import print_line_inside, print_line_outside

"""
    this dos module is preliminary and untested !
"""


class SSHDosTest(object):
    def __init__(self, cve_list_path, sshinfo_list_path):
        self.__cve_path = cve_list_path
        self.__sshinfo_path = sshinfo_list_path
        self.__exp_path_list = []
        self.__ssh_list = []
        self.__threading_list = []

    def connect(self, index):
        transport = paramiko.Transport(self.__ssh_list[index]['host'], 22)
        transport.connect(username=self.__ssh_list[index]['username'], password=self.__ssh_list[index]['passwd'])
        self.__transport = transport

    def close(self):
        self.__transport.close()

    def __upload(self, local_path, remote_path):
        sftp = paramiko.SFTPClient.from_transport(self.__transport)
        sftp.put(local_path, remote_path)
        print("Upload successfully")

    def __download(self, local_path, remote_path):
        sftp = paramiko.SFTPClient.from_transport(self.__transport)
        sftp.get(remote_path, local_path)

    def shellexec(self, command):
        ssh = paramiko.SSHClient()
        ssh._transport = self.__transport
        srdin, stdout, stderr = ssh.exec_command(command, timeout=60000)
        res, err = stdout.read().decode('utf-8'), stderr.read().decode('utf-8')
        result = res if res else err
        return result

    def __file2list(self):
        with open(self.__cve_path, 'r') as f:
            lines = f.readlines()
            for line in lines:
                line = line.strip()
                tmplist = line.split(':')
                self.__exp_path_list.append(tmplist[1])  # [0]:cve-xx-xx;[1]:path

        with open(self.__sshinfo_path, 'r') as f:
            lines = f.readlines()
            for line in lines:
                line = line.strip()
                tmplist = line.split(':')
                tmpdict = {'host': tmplist[0], 'username': tmplist[1], 'passwd': tmplist[2]}
                self.__ssh_list.append(tmpdict)  # [0]:ip;[1]username;[2]passwd

        if len(self.__exp_path_list) != len(self.__ssh_list):
            print("ERROR::the number of the exp is not equal to the number of the AP!\033[1;31m \033[0m")
            return False
        else:
            print("the list is uploaded successfully\033[1;32m \033[0m")
            return True

    def __expthreading(self, index):
        """
        single theading
        """
        remotedir = './tmp/'
        self.connect(index)
        self.shellexec('mkdir -m 755 tmp')
        print_line_outside()
        tmplist = str(self.__exp_path_list[index]).split('/')  # ./exploit/local/CVE-X-X/XXX
        cve_id = tmplist[3]
        filename = tmplist[4]
        matchres = re.search(r'.+\.py$', filename)
        remotepath = remotedir + filename
        # command stitching
        if matchres:
            shellcmd = "python3 " + remotepath
        else:
            shellcmd = remotepath
        self.shellexec('chmod +x ' + remotepath)
        self.__upload(self.__exp_path_list[index], remotepath)
        print_line_inside()
        result = self.shellexec(shellcmd)
        with open('./Log/' + cve_id + '.txt', 'w+') as f:
            f.write(result[0])
            print(cve_id + " : the output of the exp in shell is saved in " + './Log/' + cve_id + '.txt')
            print_line_inside()

        # judge whether the exp is success or not,and append the identifid information
        with open('./Log/' + cve_id + '.txt', 'a+') as f:
            if result[1] == "success":
                f.write('[cve is exist]')
            else:
                f.write('[cve is not be detacted]')
        self.close()

    def exploit(self):
        if self.__file2list():
            for i in range(0, len(self.__exp_path_list) - 1):
                tmp_thread_var = threading.Thread(target=self.__expthreading(i))
                self.__threading_list.append(tmp_thread_var)
            for each_threading in self.__threading_list:
                each_threading.start()
        else:
            print("ERROR::the function:file2list() has wrong input!\033[1;31m \033[0m")
            return


def run(cve_list_path, sshinfo_list_path):
    dos_exp = SSHDosTest(cve_list_path, sshinfo_list_path)
    dos_exp.exploit()
