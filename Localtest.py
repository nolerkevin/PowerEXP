import paramiko
import re
import time
from printsytle import print_line_inside, print_line_outside


# import uuid

class SSHLocalTest(object):
    def __init__(self, host='127.0.0.1', username='localhost', passwd='123', cvelistpath='./'):
        self.host = host  # the test machine's ip address
        self.username = username  # ssh login username
        self.passwd = passwd  # ssh login passwd
        self.port = 22  # ssh port 22
        self.cvelistpath = cvelistpath  # the exp path string
        self.__exppathlist = []  # the list save the path of exp
        self.paramter = None

    def connect(self):
        transport = paramiko.Transport(self.host, self.port)
        transport.connect(username=self.username, password=self.passwd)
        self.__transport = transport

    def close(self):
        self.__transport.close()

    def __upload(self, local_path, remote_path):
        sftp = paramiko.SFTPClient.from_transport(self.__transport)
        sftp.put(local_path, remote_path)
        print("Upload successfully")

    def __download(self, local_path, remote_path):
        sftp = paramiko.SFTPClient.from_transport(self.__transport)
        sftp.get(remote_path, local_path)

    def shellexec(self, command):
        ssh = paramiko.SSHClient()
        ssh._transport = self.__transport
        srdin, stdout, stderr = ssh.exec_command(command, timeout=60000)
        res, err = stdout.read().decode('utf-8'), stderr.read().decode('utf-8')
        result = res if res else err
        return result

    def shellexec_invoke(self, command):
        chan = self.__transport.open_session()
        chan.get_pty()
        chan.invoke_shell()
        chan.send(command + '\r')
        ret = ['']
        count = 0
        while True:
            out = chan.recv(1024)
            ret[0] = ret[0] + out.decode('utf-8').replace('\r', ' ')
            # if end with ']#' means the id is root ,done and exit
            if ']#' in out.decode('utf-8'):
                ret.append('success')
                break
            if '~]$' in out.decode('utf-8'):
                count += 1
            # if not success ,need to exit.(not including .sh files)
            if count == 2:
                ret.append('failure')
                break

        return ret

    @staticmethod
    def send_channel(self, command, channel):
        """
        this is just a test interactive shell function,maybe alternative ,maybe not

        """
        cmd = str(command) + 'r'
        result = ''
        channel.send(cmd)
        count = 1
        while '~]$' in result:
            time.sleep(0.5)
            count += 1
            print('count:', count)
            ret = channel.recv(1024)
            ret = ret.decode('utf-8')
            result += ret

    def cvepath2list(self):
        with open(self.cvelistpath, 'r') as f:
            lines = f.readlines()
            for line in lines:
                line = line.strip()
                tmplist = line.split(':')
                self.__exppathlist.append(tmplist[1])  # [0]:cve-xx-xx;[1]:path

    def exploit(self):
        """
        the main process fuction of the ssh exploit
        """
        # create the tmp directory to run exp
        self.shellexec('mkdir -m 755 tmp')

        # target directory
        remotedir = "./tmp/"

        # execute the exp programme one by one
        # and if the exp is success,the ssh pipe will be remaked to make sure the id back to ordinary permisssion
        for localpath in self.__exppathlist:
            print_line_outside()
            tmplist = str(localpath).split('/')  # ./exploit/local/CVE-X-X/XXX
            cve_id = tmplist[3]
            filename = tmplist[4]
            matchres = re.search(r'.+\.py$', filename)
            remotepath = remotedir + filename

            # command stitching
            if matchres:
                shellcmd = "python3 " + remotepath
            else:
                shellcmd = remotepath
            self.__upload(localpath, remotepath)
            print_line_inside()

            # give exp the executed permission
            self.shellexec('chmod +x ' + remotepath)
            result = self.shellexec_invoke(shellcmd)
            print("[=the execute result is as follows:=]\033[1;36m")
            print(result[0])
            print_line_inside()
            with open('./Log/' + cve_id + '.txt', 'w+') as f:
                f.write(result[0])
                print(cve_id + " : the output of the exp in shell is saved in " + './Log/' + cve_id + '.txt')
                print_line_inside()

            # judge whether the exp is success or not,and append the identifid information
            with open('./Log/' + cve_id + '.txt', 'a+') as f:
                if result[1] == "success":
                    f.write('[cve is exist]')
                else:
                    f.write('[cve is not be detacted]')


def run(cvelist_path, ipaddress, username, passwd):
    exp = SSHLocalTest(ipaddress, username, passwd, cvelist_path)
    exp.cvepath2list()
    exp.connect()
    exp.shellexec("pwd")
    exp.exploit()
    exp.close()
