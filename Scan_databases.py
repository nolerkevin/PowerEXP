import argparse
import sql_interface

parser = argparse.ArgumentParser()
parser.add_argument("-k", "--kernel", type=float, help="you should input kernel version!")
parser.add_argument("-u", "--upload", type=str, help="please input xls path!")
parser.add_argument("-s", "--show", action="store_true",help="the output info!")
args = parser.parse_args()
sql = sql_interface.sql_interfacea()
if args.kernel:
    sql.get_cve_list(args.kernel)
if args.upload:
    sql.upload_excel(args.upload)
if args.show:
    sql.get_impact_info()
