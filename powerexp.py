import argparse
import Localtest
import Dostest


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--type", type=str, required=True, choices=["local", "dos"],
                        help="choice the type you test (local or dos)")
    parser.add_argument("-f", "--file", type=str, help="give ip address in txt files(input is file path)")
    parser.add_argument("-a", "--address", type=str, help="give ip address in single parameter")
    parser.add_argument("-l", "--list", type=str, help="the payload of the cve list that you select from db")
    parser.add_argument("-u", "--username", type=str, help="the ssh login username")
    parser.add_argument("-p", "--passwd", type=str, help="the ssh login passwd")
    args = parser.parse_args()
    print(args)

    if args.type == "local":
        Localtest.run(args.list, args.address, args.username, args.passwd)
    elif args.type == "dos":
        Dostest.run(args.list, args.file)
    else:
        print("demo string")


if __name__ == "__main__":
    main()
