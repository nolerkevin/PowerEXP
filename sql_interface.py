import pymysql
import os
import re
import xlrd


class sql_interfacea:

    def __init__(self):
        self.__db = pymysql.connect(
            host='localhost',
            # host='47.96.39.43',
            user='root',
            password='123456',
            db='test'
        )
        self.__cur = self.__db.cursor()

    def __del__(self):
        self.__cur.close()
        self.__db.close()

    def __exc_sql(self, sql):
        self.__cur.execute(sql)
        return self.__cur.fetchall()

    def __isExist(self):
        """
        通过Log文件判断，成功的cve
        :return:exist_cve_List
        """
        log_path = os.path.abspath('Log')
        exist_cve_List = []
        for i in os.listdir(log_path):
            cve_path = os.path.abspath('Log/' + i)
            f = open(cve_path, 'r')
            if re.search('cve is exist', f.read()) is not None:
                exist_cve_List.append(i[:-4])
            f.close()
        return exist_cve_List

    def __write_output(self, parameter, cve_id, path):
        """
        方便后续扩展输出报告，和数据库新增的项
        :param parameter:
        :param cve_id:
        :param path:
        :return:
        """
        sql = "select " + parameter + " from cveinfo where CVE_id like '" + cve_id + "';"
        result = self.__exc_sql(sql)
        f = open(path, "a")
        if len(result) == 0:
            f.write("[+]" + cve_id + "'s information: There is no corresponding information in the database!\n\n")
        else:
            f.write("[+]" + cve_id + "'s information: " + result[0][0] + "\n\n")
        f.close()

    def get_cve_list(self, kernel):
        """
        获取内核版本目标可能存在的cve，并输出exp路径
        :param kernel: 目标内核版本
        """
        sql = "select CVE_id,path,type from cveinfo where kernel >=" + "kernel;"
        result = self.__exc_sql(sql)
        print("目标kernel:" + str(kernel) + "存在可能的cve有")
        for i in result:
            print("[!] " + i[0])
        f_cve = open(os.path.abspath('List') + "/cveList.txt", "w")
        f_dos = open(os.path.abspath('List') + "/dosList.txt", "w")
        for i in result:
            if i[2] == 'local':
                f_cve.write(i[0] + ":" + i[1] + "\n")
            elif i[2] == 'dos':
                f_dos.write(i[0] + ":" + i[1] + "\n")
        f_cve.close()
        f_dos.close()
        print("cvelist.txt path :" + os.path.abspath('List') + "/cveList.txt")
        print("doslist.txt path :" + os.path.abspath('List') + "/dosList.txt")

    def get_impact_info(self):
        """
        获取有影响的cve的info

        :return:cve impact info path   ______  *.txt path
        """
        output_path = os.path.abspath('output_test') + "/impactList.txt"
        impact_list = self.__isExist()

        if len(impact_list) == 0:
            f = open(output_path, "w")
            f.write("****************************impact cve info****************************\n\n")
            f.write("[-]No available cve is detected in this operating system !\n")
            f.write("*********************************finished******************************")
            f.close()
            return output_path
        else:
            f = open(output_path, "w")
            f.write("****************************impact cve info****************************\n\n")
            f.close()
        count = 0
        for i in impact_list:
            count += 1
            self.__write_output("info", i, output_path)
            # write_output("other", output_path)
            f = open(output_path, "a")
            if count == len(impact_list):
                f.write("*********************************finished******************************")
            else:
                f.write("***********************************************************************\n\n")
            f.close()

        print(output_path)

    def upload_excel(self,excel_path):
        """
        the Excel file must be xls , and the column should be regular,and you can see detail in code
        :return:
        """
        # f = xlrd.open_workbook('/home/luobo/Desktop/exploit-db.xls')
        f = xlrd.open_workbook(excel_path)
        table = f.sheets()[0]
        i = 0
        while i < table.nrows:
            cve_id = table.row_values(i)[0]
            type = table.row_values(i)[1]
            kernel = table.row_values(i)[2]
            path = table.row_values(i)[4]
            info = table.row_values(i)[3]
            sql = "insert into cveinfo values('" + cve_id + "','" + type + "'," + str(
                kernel) + ",'" + path + "','" + info + "');"
            try:
                self.__exc_sql(sql)
                self.__db.commit()
            except Exception as e:
                print("[-]line " + str(i + 1) + " | There is some error in your xls ,please check and try again!")
                self.__db.rollback()
                i += 1
                continue
            print("[+]line " + str(i + 1) + " | Upload data to database successfully!")
            i += 1

    def first_init_db(self):
        """
        用于之后测试，加载本地数据库
        若之后使用云数据库，则删除此方法
        :return:
        """
        sql = ""
        try:
            self.__exc_sql(sql)
            self.__db.commit()
        except Exception as e:
            print("There is some error in your sql,please check and try again!")
            self.__db.rollback()
        pass

